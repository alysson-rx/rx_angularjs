'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', [function() {
    const $ctrl = this;
    let users = [{
        id: 1,
        name: 'Alysson'
    }];

    $ctrl.getUserById = getUserById;

    function getUserById(id) {
        return users.find(i => i.id === id);
    }

}]);
