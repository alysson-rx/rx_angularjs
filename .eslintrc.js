module.exports = {
  env: {
    browser: true,
    jasmine: true,
    phantomjs: true,
    es6: true,
  },
  extends: [
    'standard',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    "semi": ["error", "always"],
    "comma-dangle": ["error", "only-multiline"],
    "no-void": 0,
  },
};
