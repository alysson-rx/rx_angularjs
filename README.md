# Avaliação de conhecimentos em Angularjs

Este projeto busca avaliar conhecimentos de candidatos à vagas para trabalhar no
clinicarx.

O desafio é fazer com que todos os testes unitários sejam bem sucedidos.

## Instalação via apt

Pré-requisitos:

* [Git](https://git-scm.com/)
* [NPM](https://git-scm.com/)

O candidato deverá clonar o repositório e, na pasta do projeto, instalar as dependencias do NPM:

```bash
git clone git@bitbucket.org:rxsaude/avaliacao-angularjs.git
cd avaliacao-angularjs
npm install
npm start
```

Para que os testes envolvendo os componentes de listagem de dados funcionem, 
é necessário que o o banco de dados funcionando corretamente. O banco de dados escolhido 
para este projeto é o Sqlite3, devido à simplicidade dele. 
O NPM lida com a compilação do executável.

## Executando os testes unitários

Na pasta do projeto, execute o comando:

```
npm run test-single-run
```

## Tarefas

Lista de correções e melhorias necessárias.

**\routes\parties\PartyViewComponent**:

- Formate a data de fundação do Partido para o padrão "dia/mês/ano".

- Formate a quantidade de filiados para o padrão brasileiro.

- Corrigir as classes da lista de partidos para que seja exibido corretamente em 
resolução mobile (quando largura da tela é inferior a 400px).

**\routes\parties\PartiesListComponent**:

- Refatorar o mecanismo de busca de partidos para que a API retorne apenas 
partidos que a sigla comece com o termo digitado no input de busca.

- Implementar um paginador que permita o usuário navegar pelos resultados API.

**\layout\FooterComponent**:

- Crie um novo componente chamado FooterComponent (e seu respectivo teste unitário).
O template deste componente deve conter um 
[sticky footer](https://getbootstrap.com/docs/4.0/examples/sticky-footer/).
A mensagem do footer deverá conter a seguinte frase:

  > Fonte dos dados: [brasil.io](https://brasil.io), data: 01/01/2020.

  O teste unitário do componente deverá testar se a frase está sempre de acordo 
  com o requisito acima.

## Conclusão

Durante a realização das tarefas, as alterações devem ser commitadas, para criar
uma linha do tempo compreensível no histórico do git.

Quando todos os requisitos estiverem satisfeitos, o repositório deve
ser encapsulado num arquivo bundle.

```
git bundle create rx_angularjs.bundle master
```

O arquivo gerado (`rx_angularjs.bundle`) deve ser enviado para o email: 
[alysson@clinicarx.com.br](mailto:alysson@clinicarx.com.br)
