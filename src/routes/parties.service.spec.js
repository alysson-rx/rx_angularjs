import angular from 'angular';
import 'angular-mocks/angular-mocks';
import { RoutesModule } from './index';
import { PartiesFactoryTag } from './parties.service';

describe('Factory ' + PartiesFactoryTag, () => {
  let $httpBackend;
  let $factory;
  let $injector;

  beforeEach(angular.mock.module(RoutesModule));
  beforeEach(angular.mock.inject((_$injector_) => {
    $injector = _$injector_;
    $httpBackend = $injector.get('$httpBackend');
    $factory = $injector.get(PartiesFactoryTag);
  }));

  it('should be defined', function () {
    expect($factory).toBeDefined();
  });

  it('should call "query(param)" and return parsed json', function () {
    const [page, offset, limit] = [2, 10, 20];
    $httpBackend.expectGET(`/api/partidos?page=${page}&offset=${offset}&limit=${limit}`)
      .respond('{"data":[{"sigla":"AVANTE"},{"sigla":"CIDADANIA"}]}');

    const param = { page: page, offset: offset, limit: limit };
    const query = $factory.query(param);
    query.$promise.then(response => {
      expect(response.data).toBeDefined();
      expect(response.data.length).toBe(2);
    });
    $httpBackend.flush();
  });

  it('should call "get(sigla)" and return parsed json', function () {
    const sigla = 'NOVO';
    $httpBackend.expectGET(`/api/partidos/${sigla}`)
      .respond('{"data":{"sigla":"NOVO"}}');

    const query = $factory.get(sigla);
    query.$promise.then(response => {
      expect(response.data).toBeDefined();
      expect(response.data.sigla).toBeDefined();
    });
    $httpBackend.flush();
  });
});
