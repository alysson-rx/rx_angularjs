import angular from 'angular';
import ngResource from 'angular-resource';
// console.log(RoutesModule);
class PartiesService {
  constructor ($resource) {
    this.resource = $resource('/api/partidos/:sigla', {}, {
      query: {
        method: 'GET',
        params: { filter: null, page: 1, offset: 0, limit: 10 },
        isArray: false,
        transformResponse: function (response) {
          // Get the data from the response object
          return JSON.parse(response);
        }
      },
      get: {
        method: 'GET',
        isArray: false,
        transformResponse: function (response) {
          // Get the data from the response object
          return JSON.parse(response);
        }
      }
    });
  }

  query (params) {
    return this.resource.query(params);
  }

  get (id) {
    return this.resource.get({ sigla: id });
  }

  static serviceFactory ($resource) {
    return new PartiesService($resource);
  }
}

PartiesService.serviceFactory.$inject = ['$resource'];

export const PartiesFactoryTag = 'PartiesFactory';
export const PartiesFactory = angular
  .module('rxApp.routes.parties', [
    ngResource,
  ])
  .factory(PartiesFactoryTag, PartiesService.serviceFactory)
  .name;
