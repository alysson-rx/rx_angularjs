import angular from 'angular';
import './main.component.scss';
import { HeaderComponent } from './header.component';

class MainController {
  constructor ($rootScope) {
    this.isWorking = 'yes';

    $rootScope.search = this.search = { input: '' };
  }
}

export const MainTag = 'rxMain';
export const MainComponent = angular.module('rxApp.layout.main', [HeaderComponent])
  .component(MainTag, {
    template: require('./main.component.html').default,
    controller: MainController,
    controllerAs: '$ctrl',
    transclude: true,
  })
  .name;
